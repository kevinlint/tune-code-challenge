// Combines user and log data and generates public/data/summaries.json file

const fs = require('fs');
const userData = require('./data/users.json');
const logData = require('./data/logs.json');

const summaries = () => {
  // Loop users object and initialize totals
  let users = userData.reduce(
    (acc, curr) => ({
      ...acc,
      [curr.id]: {
        ...curr,
        totalImpressions: 0,
        totalConversions: 0,
        totalRevenue: 0,
        conversionsPerDay: {}
      }
    }),
    {}
  );

  // Loop log data and populate totals
  logData.forEach(log => {
    const user = users[log.user_id];
    if (log.type === 'impression')
      user.totalImpressions = user.totalImpressions + 1;
    if (log.type === 'conversion')
      user.totalConversions = user.totalConversions + 1;
    if (log.type === 'conversion') {
      const day = log.time.split(' ')[0];
      if (!user.conversionsPerDay.hasOwnProperty(day)) {
        user.conversionsPerDay[day] = 0;
      } else {
        user.conversionsPerDay[day] += 1;
      }
    }
    user.totalRevenue += log.revenue;
  });

  // loop users object and sort conversions per day
  Object.entries(users).forEach(([, user]) => {
    const orderedConversionsPerDay = [];
    Object.keys(user.conversionsPerDay)
      .sort()
      .forEach(function(key) {
        orderedConversionsPerDay.push([key, user.conversionsPerDay[key]]);
      });
    user.conversionsPerDay = orderedConversionsPerDay;
  });

  return Object.entries(users).reduce((acc, [, v]) => [...acc, v], []);
};

// console.log(summaries());

fs.writeFileSync(
  './public/data/summaries.json',
  JSON.stringify(summaries()),
  'utf-8'
);

console.log('./public/data/summaries.json file was created');
