import React, { useContext } from 'react';
import { Card, Header, Grid, Sticky, SortForm } from '../parts';
import { array_move } from '../lib/util';
import sortUsers from '../lib/sortPersonnel';
import { PersonnelContext } from '../lib/Store';
import { NAME, IMPRESSIONS, CONVERSIONS, REVENUE } from '../config/constants';

const Dashboard = () => {
  const [
    personnel,
    setPersonnel,
    sortBy,
    setSortBy,
    order,
    setOrder,
    gridSort,
    setGridSort
  ] = useContext(PersonnelContext);
  const users = gridSort ? personnel : sortUsers(personnel, sortBy, order);
  return (
    <>
      <Sticky>
        <Header>
          <SortForm
            options={[NAME, IMPRESSIONS, CONVERSIONS, REVENUE]}
            onSortByChange={val => {
              setGridSort(false);
              setSortBy(val);
              setPersonnel(sortUsers(users, val, order));
            }}
            onOrderChange={val => {
              setGridSort(false);
              setOrder(val);
              setPersonnel(sortUsers(users, sortBy, val));
            }}
          />
        </Header>
      </Sticky>
      <div style={{ margin: 20 }}>
        <Grid
          items={users}
          Component={Card}
          onSort={({ oldIndex, newIndex }) => {
            setGridSort(true);
            setPersonnel([...array_move(users, oldIndex, newIndex)]);
          }}
        />
      </div>
    </>
  );
};
Dashboard.defaultProps = {};
export default Dashboard;
