import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './Dashboard';
import Store from '../lib/Store';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Store>
      <Dashboard />
    </Store>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
