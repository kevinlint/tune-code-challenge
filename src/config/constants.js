export const NAME = 'name';
export const IMPRESSIONS = 'impressions';
export const CONVERSIONS = 'conversions';
export const REVENUE = 'revenue';
export const ASC = 'asc';
export const DESC = 'desc';
