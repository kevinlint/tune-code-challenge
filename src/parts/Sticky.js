import React, { useRef, useState, useCallback } from "react";
import useEventListener from "../lib/useEventListener";

const Sticky = props => {
  const container = useRef(null);
  const [sticky, setSticky] = useState(false);
  const handler = useCallback(() => {
    setSticky(window.pageYOffset > container.current.offsetHeight);
  }, [setSticky]);
  useEventListener("scroll", handler, window);
  return (
    <div ref={container} className={`${sticky && "sticky"}`}>
      {props.children}
    </div>
  );
};

export default Sticky;
