import React from "react";

const ConversionChart = props => {
  const max = Math.max.apply(Math, props.points);
  const stepX = props.width / props.points.length;
  const getY = point => (point / max) * props.height;
  const points = props.points
    .map((point, idx) => `${idx * stepX},${getY(point)}`)
    .join(" ");
  const pointsFrom = props.points
    .map((point, idx) => `${idx * stepX},${props.height / 2}`)
    .join(" ");
  return (
    <svg
      width={props.width}
      height={props.height}
      viewBox={`0 0 ${props.width} ${props.height}`}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <polyline
        points={points}
        style={{ fill: "transparent", stroke: "#bbbbbb", strokeWidth: 1 }}
      >
        <animate
          attributeName="points"
          dur="1s"
          repeatCount="1"
          from={pointsFrom}
          to={points}
        />
      </polyline>
    </svg>
  );
};

ConversionChart.defaultProps = {
  points: [],
  width: 150,
  height: 50
};

export default ConversionChart;
