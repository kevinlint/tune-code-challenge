import React from "react";
import "./Header.css";

const Header = props => <div className="header">{props.children}</div>;
Header.defaultProps = {};

export default Header;
