import React from 'react';
import './Loading.css';

const Loading = props => (
  <div className="loading">
    <div className="loading--logo" />
    <div className="loading--animation" />
  </div>
);
Loading.defaultProps = {};

export default Loading;
