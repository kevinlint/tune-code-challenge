import React from "react";
import "./Grid.css";
import { SortableContainer, SortableElement } from "react-sortable-hoc";

const GridItem = SortableElement(({ children }) => (
  <li className="grid--item">{children}</li>
));

const GridList = SortableContainer(({ children }) => (
  <ul className="grid">{children}</ul>
));

const Grid = props => {
  return (
    <GridList
      axis="xy"
      transitionDuration={200}
      helperClass="grid--sort--item"
      onSortEnd={props.onSort}
    >
      {props.items.map((item, index) => (
        <GridItem key={`item-${index}`} index={index}>
          <props.Component key={`item-${item.id}`} {...item} />
        </GridItem>
      ))}
    </GridList>
  );
};

Grid.defaultProps = {
  items: [],
  onSort: null
};

export default Grid;
