import React from 'react';
import ReactDOM from 'react-dom';
import ConversionChart from './ConversionChart';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ConversionChart />, div);
  ReactDOM.unmountComponentAtNode(div);
});
