import React from 'react';
import Avatar from './Avatar';
import ConversionChart from './ConversionChart';
import './Card.css';

const formatDateAsMonthDay = dateString => {
  const d = new Date(dateString);
  return `${d.getMonth() + 1}/${d.getDate() + 1}`;
};

const Card = props => (
  <article className="card">
    <div className="card__header">
      <header className="card__header__inner">
        <div className="card__avatar">
          <Avatar image={props.avatar} size={50} title={props.name} />
        </div>
        <div className="card__title">
          <h1 className="card__name">{props.name}</h1>
          <p className="card__occupation">{props.occupation}</p>
        </div>
      </header>
    </div>
    <div className="card__metrics">
      <div className="card__conversion__chart">
        <ConversionChart points={props.conversionsPerDay.map(i => i[1])} />
        <label>
          Conversions:{' '}
          {props.conversionsPerDay.length > 0 &&
            formatDateAsMonthDay(props.conversionsPerDay[0][0])}{' '}
          -{' '}
          {props.conversionsPerDay.length > 0 &&
            formatDateAsMonthDay(
              props.conversionsPerDay[props.conversionsPerDay.length - 1][0]
            )}
        </label>
      </div>
      <div className="card__conversion__numbers">
        <div className="card__conversion__impressions">
          {props.totalImpressions}
          <label>impressions</label>
        </div>
        <div className="card__conversion__conversions">
          {props.totalConversions}
          <label>conversions</label>
        </div>
        <div className="card__conversion__revenue">
          $
          {
            props.totalRevenue
              .toFixed(2)
              .replace(/\d(?=(\d{3})+\.)/g, '$&,')
              .split('.')[0]
          }
        </div>
      </div>
    </div>
  </article>
);

Card.defaultProps = {
  key: null,
  avatar: null,
  name: null,
  occupation: null,
  conversionsPerDay: [],
  totalImpressions: 0,
  totalConversions: 0,
  totalRevenue: 0
};

export default Card;
