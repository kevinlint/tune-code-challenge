import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { stringToHslColor } from '../lib/util';
import SvgLetter from './SvgLetter';
import './Avatar.css';

const Avatar = props => {
  const svgString = encodeURIComponent(
    renderToStaticMarkup(
      <SvgLetter letter={props.title && props.title.charAt(0)} />
    )
  );
  const dataUri = `url("data:image/svg+xml,${svgString}")`;
  return (
    <div
      className="avatar"
      style={{
        backgroundImage: `url(${props.image}), ${dataUri}`,
        backgroundColor: props.title && stringToHslColor(props.title, 50, 50),
        borderRadius: props.size,
        width: props.size,
        height: props.size
      }}
    />
  );
};

Avatar.defaultProps = {
  size: 100,
  image: '',
  title: null
};
export default Avatar;
