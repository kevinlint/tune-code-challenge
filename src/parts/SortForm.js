import React from "react";
import "./SortForm.css";

const SortForm = props => (
  <div className="sort">
    <label>Sort</label>
    <div className="select" style={{ marginRight: 10 }}>
      <select onChange={e => props.onSortByChange(e.target.value)}>
        {props.options.map(i => (
          <option key={`option-${i}`}>{i}</option>
        ))}
      </select>
    </div>
    <div className="select">
      <select onChange={e => props.onOrderChange(e.target.value)}>
        <option>asc</option>
        <option>desc</option>
      </select>
    </div>
  </div>
);

SortForm.defaultProps = {
  options: [],
  onSortByChange: null,
  onOrderChange: null
};

export default SortForm;
