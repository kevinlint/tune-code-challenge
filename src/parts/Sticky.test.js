import React from 'react';
import ReactDOM from 'react-dom';
import Sticky from './Sticky';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Sticky />, div);
  ReactDOM.unmountComponentAtNode(div);
});
