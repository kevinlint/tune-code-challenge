import React from "react";
const SvgLetter = props => (
  <svg
    width={props.width}
    style={props.style}
    height={props.height}
    fill={props.fill}
    viewBox={props.viewBox}
    className={props.className}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <text textAnchor="middle" x="50" y="73" style={props.style}>
      {props.letter}
    </text>
  </svg>
);

SvgLetter.defaultProps = {
  style: { fontSize: "2rem", fontFamily: "verdana" },
  fill: "#FFF",
  width: "100%",
  className: "",
  height: "100%",
  viewBox: "0 0 100 100",
  letter: ""
};

export default SvgLetter;
