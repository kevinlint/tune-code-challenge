import React from 'react';
import ReactDOM from 'react-dom';
import SortForm from './SortForm';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SortForm />, div);
  ReactDOM.unmountComponentAtNode(div);
});
