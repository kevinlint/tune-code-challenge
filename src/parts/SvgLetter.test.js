import React from 'react';
import ReactDOM from 'react-dom';
import SvgLetter from './SvgLetter';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SvgLetter />, div);
  ReactDOM.unmountComponentAtNode(div);
});
