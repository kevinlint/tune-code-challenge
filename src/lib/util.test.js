import { stringToHslColor, chunk, array_move } from './util';

it('converts string to hsl', () => {
  expect(stringToHslColor('Amber', 50, 50)).toEqual('hsl(187, 50%, 50%)');
});

it('chunks arrays', () => {
  expect(chunk([1, 2, 3, 4, 5, 6], 2)).toEqual([[1, 2], [3, 4], [5, 6]]);
});

it('moves array elements', () => {
  expect(array_move(['A', 'B', 'C'], 1, 0)).toEqual(['B', 'A', 'C']);
});
