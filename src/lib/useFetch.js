import { useState, useEffect } from 'react';
import axios from 'axios';

const useFetch = (url, staticData = []) => {
  const [data, setData] = useState(staticData);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if (staticData) {
      setData(staticData);
      setLoading(false);
    } else {
      setLoading(true);
      axios(url).then(result => {
        setData(result.data);
        setLoading(false);
      });
    }
  }, [staticData, url]);
  return [data, setData, loading];
};

export default useFetch;
