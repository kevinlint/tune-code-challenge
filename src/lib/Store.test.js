import React, { useContext } from 'react';
import { shallow } from 'enzyme';
import Store, { PersonnelContext } from './Store';

const TestComponent = () => {
  const [personnel] = useContext(PersonnelContext);
  return <>{personnel.join(',')}</>;
};

it('propagates store context', () => {
  const wrapper = shallow(
    <Store personnel={[1, 2, 3]} loading={false}>
      <TestComponent />
    </Store>
  );
  expect(wrapper.html()).toEqual('1,2,3');
});
