import {
  NAME,
  IMPRESSIONS,
  CONVERSIONS,
  REVENUE,
  ASC
} from '../config/constants';

export default (users, sortBy, order) => {
  const orderOperator = order === ASC ? 1 : -1;
  switch (sortBy) {
    case NAME:
      return users.sort((a, b) =>
        a.name < b.name ? -1 * orderOperator : 1 * orderOperator
      );
    case IMPRESSIONS:
      return users.sort((a, b) =>
        a.totalImpressions < b.totalImpressions
          ? -1 * orderOperator
          : 1 * orderOperator
      );
    case CONVERSIONS:
      return users.sort((a, b) =>
        a.totalConversions < b.totalConversions
          ? -1 * orderOperator
          : 1 * orderOperator
      );
    case REVENUE:
      return users.sort((a, b) =>
        a.totalRevenue < b.totalRevenue ? -1 * orderOperator : 1 * orderOperator
      );
    default:
      return users;
  }
};
