import sortPersonnel from './sortPersonnel';
import {
  NAME,
  IMPRESSIONS,
  CONVERSIONS,
  REVENUE,
  ASC,
  DESC
} from '../config/constants';

const mockSummaries = [
  {
    name: 'Sofia S. Parkin',
    avatar: '',
    id: 1,
    occupation: 'Terrazzo worker',
    totalImpressions: 912,
    totalConversions: 320,
    totalRevenue: 16037.780000000006,
    conversionsPerDay: [
      ['2013-04-09', 9],
      ['2013-04-10', 10],
      ['2013-04-11', 5]
    ]
  },
  {
    name: 'Jason P. Varela',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/soyjavi/128.jpg',
    id: 2,
    occupation: 'Ticket seller',
    totalImpressions: 911,
    totalConversions: 305,
    totalRevenue: 15448.249999999996,
    conversionsPerDay: [
      ['2013-04-09', 5],
      ['2013-04-10', 9],
      ['2013-04-11', 10]
    ]
  },
  {
    name: 'Tracy W. Chin',
    avatar: '',
    id: 3,
    occupation: 'Clerk',
    totalImpressions: 901,
    totalConversions: 280,
    totalRevenue: 13673.149999999981,
    conversionsPerDay: [
      ['2013-04-09', 8],
      ['2013-04-10', 11],
      ['2013-04-11', 13]
    ]
  }
];

it('sorts by name in ascending order', () => {
  const sortByName = sortPersonnel(mockSummaries, NAME, ASC);
  expect(sortByName[0].id).toEqual(2);
});

it('sorts by name in descending order', () => {
  const sortByName = sortPersonnel(mockSummaries, NAME, DESC);
  expect(sortByName[0].id).toEqual(3);
});

it('sorts by impressions in ascending order', () => {
  const sortByName = sortPersonnel(mockSummaries, IMPRESSIONS, ASC);
  expect(sortByName[0].id).toEqual(3);
});

it('sorts by impressions in descending order', () => {
  const sortByName = sortPersonnel(mockSummaries, IMPRESSIONS, DESC);
  expect(sortByName[0].id).toEqual(1);
});

it('sorts by conversions in ascending order', () => {
  const sortByName = sortPersonnel(mockSummaries, CONVERSIONS, ASC);
  expect(sortByName[0].id).toEqual(3);
});

it('sorts by conversions in descending order', () => {
  const sortByName = sortPersonnel(mockSummaries, CONVERSIONS, DESC);
  expect(sortByName[0].id).toEqual(1);
});

it('sorts by revenue in ascending order', () => {
  const sortByName = sortPersonnel(mockSummaries, REVENUE, ASC);
  expect(sortByName[0].id).toEqual(3);
});

it('sorts by revenue in descending order', () => {
  const sortByName = sortPersonnel(mockSummaries, REVENUE, DESC);
  expect(sortByName[0].id).toEqual(1);
});
