import React, { useState } from 'react';
import useFetch from './useFetch';
import { NAME, ASC } from '../config/constants';
import { Loading } from '../parts';

export const PersonnelContext = React.createContext('test');

const Store = props => {
  const [personnel, setPersonnel, loading] = useFetch(
    props.dataUrl,
    props.personnel
  );
  const [sortBy, setSortBy] = useState(NAME);
  const [order, setOrder] = useState(ASC);
  const [gridSort, setGridSort] = useState(false);
  if (loading && props.loading) return <Loading />;
  return (
    <PersonnelContext.Provider
      value={[
        personnel,
        setPersonnel,
        sortBy,
        setSortBy,
        order,
        setOrder,
        gridSort,
        setGridSort
      ]}
    >
      {props.children}
    </PersonnelContext.Provider>
  );
};

Store.defaultProps = {
  personnel: null,
  dataUrl: '/data/summaries.json',
  loading: true
};

export default Store;
