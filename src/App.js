import React from 'react';
import './App.css';
import { Dashboard } from './pages';
import Store from './lib/Store';

function App() {
  return (
    <Store>
      <Dashboard />
    </Store>
  );
}

export default App;
