# Tune Code Challenge

by Kevin Lint (https://github.com/kvnlnt)

## Setup

`yarn`

## Launch

`yarn start`

## Test

`yarn test`

## Data

To minimize scope but still illustrate a fully functioning client application I created a data view file from the two supplied data source files. This produced a ~80kb file to be served from `./public/data/summaries.json`. The data view file is produced from the `./task/generateSummaries.js` script which can be run with `npm run generateSummaries`.

To update test data:

1. replace the files in `./tasks/data`
2. `npm run generateSummaries`
3. `yarn start`
